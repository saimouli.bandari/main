import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-side-menus',
  standalone: true,
  imports: [
    CommonModule
  ],
  templateUrl: './side-menus.component.html',
  styleUrl: './side-menus.component.scss'
})
export class SideMenusComponent {
  menusData: any[] = [
    {
      name: "Dashboard",
      path: "/pages/app-one/dashboard"
    },
    {
      name: "Live",
      path: "/pages/app-one/live-page"
    },
    {
      name: "Test",
      path: "/pages/test-cmp"
    }
  ]
  activePath = "";

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.getCurrentPagePath()
  }

  /**
   * This function gets the current page path on initial load and updates it after routing changes.
   */
  getCurrentPagePath() {
    /**
     * on initial load it will get the current path from the inside of the pages module
     */
    try {
      this.activePath = window.location.pathname
    } catch { }
    /**
     * after routing it will check for the change of the router menu and update the current path
     */
    try {
      this.router.events.subscribe(val => {
        if (val instanceof NavigationEnd) {
          this.activePath = window.location.pathname
        }
      })
    } catch { }
  }

  navigateTo(menu: any) {
    this.router.navigate([menu.path])
  }

}
