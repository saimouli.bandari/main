import { Component } from '@angular/core';
import { HeaderComponent } from '../components/header/header.component';
import { RouterOutlet } from '@angular/router';
import { SideMenusComponent } from '../components/side-menus/side-menus.component';

@Component({
  selector: 'app-pages',
  standalone: true,
  imports: [
    HeaderComponent,
    SideMenusComponent,
    RouterOutlet
  ],
  templateUrl: './pages.component.html',
  styleUrl: './pages.component.scss'
})
export class PagesComponent {

}
