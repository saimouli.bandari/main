import { Routes } from '@angular/router';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

export const routes: Routes = [
    {
        path: "auth",
        loadChildren: () => import("auth/routes").then(m => m.routes).catch(e => console.error(e))
    },
    {
        path: "pages",
        loadChildren: () => import("./pages/pages.routes").then(r => r.routes)
    },
    {
        path: "page-not-found",
        component: PageNotFoundComponent
    },
    {
        path: "",
        redirectTo: "auth",
        pathMatch: "full"
    },
    {
        path: "**",
        redirectTo: "page-not-found"
    }
];
